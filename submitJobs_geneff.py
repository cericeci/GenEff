#!/usr/bin/env python
import os, re
import commands
import math, time
import sys

print 
print 'START'
print 
########   YOU ONLY NEED TO FILL THE AREA BELOW   #########
########   customization  area #########
FileList        = sys.argv[1]         
queue           = sys.argv[2] 
tag             = sys.argv[3]
NumberOfJobs    = int(sys.argv[4]) # -1 == run all
doSubmit = False
tag = tag 
path = os.getcwd()
print
print 'do not worry about folder creation:'
os.system("rm -rf tmp%s"%tag)
os.system("rm -rf exec%s"%tag)
os.system("rm -rf batchlogs%s"%tag)
os.system("mkdir tmp%s"%tag)
os.system("mkdir exec%s"%tag)
print

files = []
comms = open("testbase.sh", "r")
lines = comms.readlines()
for f in os.listdir(FileList):
  if not("py" in f): continue
  if "pyc" in f: continue
  if "__init__" in f: continue
  files.append(FileList + "/" + f)
if NumberOfJobs == -1: 
  NumberOfJobs = (len(files))
##### loop for creating and sending jobs #####
for x in range(1, int(NumberOfJobs)+1):
    ##### creates directory and file list for job #######
    jobFiles = files[x-1]
    ##### creates jobs #######
    #print(FullOutputDir+OutputFileNames+"_"+str(x)+".root")
    if len(jobFiles) < 1: continue
    with open('exec%s/job_'%tag+str(x)+'.sh', 'w') as fout:
        short = jobFiles.split("/")[-1].replace(".py","")
        fout.write("#!/bin/sh\n")
        fout.write("echo\n")
        fout.write("echo 'START---------------'\n")
        fout.write("echo %s\n"%short)
        fout.write("echo 'WORKDIR ' ${PWD}\n")
        fout.write("export HOME=$PWD\n")
        fout.write("source /afs/cern.ch/cms/cmsset_default.sh\n")
        fout.write("cd %s\n"%path)
        fout.write("cmsenv\n")
        for l in lines:
          fout.write(l.replace("[[FRAGMENT]]", jobFiles).replace("[[EVENTS]]", str(10000)).replace("[[SHORT]]", short))
        fout.write("echo 'STOP---------------'\n")
        fout.write("echo\n")
        fout.write("echo\n")
    os.system("chmod 755 exec%s/job_"%tag+str(x)+".sh")
    
###### create submit.sub file ####
    
os.mkdir("batchlogs%s"%tag)
with open('submit.sub', 'w') as fout:
    fout.write("executable              = $(filename)\n")
    fout.write("arguments               = $(Proxy_path) $(ClusterId)$(ProcId)\n")
    fout.write("output                  = batchlogs%s/$(ClusterId).$(ProcId).out\n"%tag)
    fout.write("error                   = batchlogs%s/$(ClusterId).$(ProcId).err\n"%tag)
    fout.write("log                     = batchlogs%s/$(ClusterId).log\n"%tag)
    fout.write('+JobFlavour = "%s"\n' %(queue))
    fout.write("\n")
    fout.write("queue filename matching (exec%s/job_*sh)\n"%tag)
    
###### sends bjobs ######
os.system("echo submit.sub")
if doSubmit: os.system("condor_submit submit.sub")
  
print
print "your jobs:"
os.system("condor_q")
print
print 'END'
print
