## Setup

Run install.sh to get the proper cmssw setup

Copy all files inside of $CMSSW_BASE/src/

Modify basefragment.py accordingly to the process you want to test (usually copying a fragment from mcm will do the trick)

Modify generateFragments.py as needed to accommodate for your scan definition and the free parameters of the request

Run submitJobs_geneff.py in order to submit the jobs (one validation job per fragment) 
