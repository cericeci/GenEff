base = open("basefragment.py","r")
inText = base.read()

def widthtable(dm):
    if dm==10: 
        return 3.250000000E-17
    elif dm==15: 
        return 3.520000000E-15
    elif dm==20: 
        return 5.860000000E-14
    elif dm==25: 
        return 4.480000000E-13
    elif dm==30: 
        return 2.240000000E-12
    else: -1.0

def getMatchingParameters(mass):
    if mass>99 and mass<199: return 62., 0.498
    elif mass<299: return 62., 0.361
    elif mass<399: return 62., 0.302
    elif mass<499: return 64., 0.275
    elif mass<599: return 64., 0.254
    elif mass<1299: return 68., 0.237
    elif mass<1451: return 70., 0.243
    elif mass<1801: return 74., 0.246
    elif mass<2001: return 76., 0.267
    elif mass<2201: return 78., 0.287
    elif mass<2601: return 80., 0.320
    elif mass<2801: return 84., 0.347
    else: return 84., 0.347
    
dmVals    = {10: 250, 15:75, 20:75, 25:75, 30:75} # 5 dM points
mStopVals = range(250, 1101, 25)                  # 35 mstop points 
brforctaus= [0.3 , 1.0] # Two different hypothesis for ctau in BR, for reweighting
points = []

for dm in dmVals.keys(): 
  for m in mStopVals:
     for brct in brforctaus:
       points.append([m, dm, brct, dmVals[dm]]) #mstop, dm, events in thousands

for point in points:
  # Some paremeters
  mstop = point[0]
  mlsp  = point[0] - point[1]
  brforct = point[2]
  Width_4Body_500 = widthtable(mstop-mlsp)
  Width_4Body = Width_4Body_500 * (500.0/mstop)
  BR_4 = 80.0E-02
  BR_2 = 100.0E-02 - BR_4


  ## Width_tot = Width_4Body/BR_4 ## This is the realistic case

  Width_tot = Width_4Body/brforct #  This is going to be reweighted, so here BR=1.0 or 0.3
  hcut = 6.5821E-25#GeV
  c = 2.9979E11 #mm/s

  ctau = (hcut/Width_tot) * c
  
  qcut, tru_eff = getMatchingParameters(mstop)
  print("fragments/mStop%i_mN1%i_CTau%1.1f_Width%1.3f_BR2%1.1f_BR4%1.1f_QCUT%2.2f_EFF%1.3f.py"%(mstop, mlsp, brforct, Width_tot, BR_2, BR_4, qcut, tru_eff))
  out = open(("fragments/mStop%i_mN1%i_CTau%1.1f_Width%1.3f_BR2%1.1f_BR4%1.1f_QCUT%2.2f_EFF%1.3f.py"%(mstop, mlsp, brforct, Width_tot, BR_2, BR_4, qcut, tru_eff)).replace(".", "p").replace("ppy",".py"), "w") 
  out.write(inText.replace("[[MSTOP]]", str(mstop)).replace("[[MLSP]]", str(mlsp)).replace("[[WIDTH_TOT]]", str(Width_tot)).replace("[[BR_2]]", str(BR_2)).replace("[[BR_4]]", str(BR_4)).replace("[[QCUT]]", str(qcut)).replace("[[TRU_EFF]]",str(tru_eff)))

